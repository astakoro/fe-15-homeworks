const burgerMenu = document.querySelector('.hamburger');
const closeButton = document.querySelector('.close');
const navBar = document.querySelector('.nav-bar');

burgerMenu.addEventListener('click', ()=> {
    burgerMenu.classList.add('hidden-class');
    navBar.classList.remove('hidden-class');
});

closeButton.addEventListener('click', ()=> {
    navBar.classList.add('hidden-class');
    burgerMenu.classList.remove('hidden-class');
});