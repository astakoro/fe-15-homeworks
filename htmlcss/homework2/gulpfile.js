const {src, dest, series, parallel, watch} = require('gulp'); // added watch here
const gulp = require('gulp');
const sass = require('gulp-sass');
sass.compiler = require('node-sass');
const postcss = require('gulp-postcss');
const uncss = require('postcss-uncss');
const del = require('del');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const concatCss = require('gulp-concat-css');
const concat = require("gulp-concat");
const uglify = require('gulp-uglify');
const imagemin = require('gulp-imagemin');
const pipeline = require('readable-stream').pipeline;
const browserSync = require('browser-sync').create();


function copyHtml() {
    return src('./index.html')
        .pipe(dest('./dist'))
        .pipe(browserSync.reload({ stream: true })); // added this line
}

function copyJs() {
    return pipeline(
        gulp.src('./src/js/**.js'),
            concat("scripts.min.js"),
        uglify()
    )

        .pipe(dest('./dist/js'))
        .pipe(browserSync.reload({ stream: true })); // added this line
}

function optimizeImages(){
    return src('./src/img/*')
        .pipe(imagemin())
        .pipe(dest('./dist/img'))
}


function buildScss() {
    return src('./src/scss/*.scss')
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(postcss([
            uncss({html: ['./dist/index.html']})]))
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(concatCss("./styles.min.css"))
        .pipe(cleanCSS())
        .pipe(dest('./dist/css'))
        .pipe(browserSync.stream());  // added this line
}

function copyAssets() {
    return src('./src/assets/**')
        .pipe(dest('./dist/assets'))
}

function emptyDist() {
    return del('./dist/**');
}

// moved build configuration to variable
const build = series(
    emptyDist,
    parallel(
        series(copyHtml , buildScss),
        copyAssets,
        copyJs,
        optimizeImages
    )
);

// added this function to start server and watch for index.html, scss, js
function serve() {
    browserSync.init({
        server: './dist'
    });

    watch(['./index.html'], copyHtml);
    watch(['./src/scss/*.scss'], buildScss);
    watch(['./src/js/*.js'], copyJs);
}

exports.html = copyHtml;
exports.scss = buildScss;
exports.assets = copyAssets;
exports.clear = emptyDist;
exports.js = copyJs;
exports.optimize = optimizeImages;
exports.build = build; // changed here with variable from line 40
exports.default = series(build, serve); // added this
