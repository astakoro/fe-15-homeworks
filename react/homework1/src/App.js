import React, {Component} from 'react';
import Button from './components/UI/Button/Button'
import Modal from './components/Modal/Modal';


class App extends Component {
  state = {
    isModal: false,
    currentModal: null
  };

  deleteModalOpenWindow() {
    this.setState({
      isModal: true,
      currentModal: 'deleteModal'
    })
  }

  successModalOpenWindow() {
    this.setState({
      isModal: true,
      currentModal: 'successModal'
    })
  }

  modalCloseHandler() {
    this.setState({
      isModal: !this.state.isModal,
      currentModal: null
    })
  }

  render() {
    const modalOne = {
      deleteModal: {
        type: 'delete',
        header: 'Вы действительно хотите удалить этот файл?',
        text: `Если Вы удалить этот файл, востановление будет не возможно. Вы уверены?`,
        closeButton: true,
        actions: [
          <Button
            key={0}
            text={'OK'}
            backgroundColor={'#268E00'}
            onClick={this.modalCloseHandler.bind(this)}
          />,
          <Button
            key={1}
            text={'Отмена'}
            backgroundColor={'#268E00'}
            onClick={this.modalCloseHandler.bind(this)}
          />
        ]
      },
      successModal: {
        type: 'success',
        header: 'Супер',
        text: `Поздравляем! Вы справились!`,
        closeButton: true,
        actions: [
          <Button
            key={0}
            text={'OK'}
            backgroundColor={'#6BEC3B'}
            onClick={this.modalCloseHandler.bind(this)}
          />,
        ]}
    }
    const cnfType = modalOne[this.state.currentModal]

    const modal = this.state.isModal &&
      <Modal
        type={cnfType.type}
        header={cnfType.header}
        text={cnfType.text}
        actions={cnfType.actions}
        closeButton={cnfType.closeButton}
        closeHandler={this.modalCloseHandler.bind(this)}
      />;
    
    return (
      <div className={'btn'}>
        <Button
          text={'Успех'}
          backgroundColor={'#95EE6B'}
          onClick={this.successModalOpenWindow.bind(this)}
        />
        <Button
          text={'Удалить'}
          backgroundColor={'#A60000'}
          onClick={this.deleteModalOpenWindow.bind(this)}
        />
        {modal}
      </div>
    )
  }
}

export default App;
