const req = new XMLHttpRequest();
req.open("GET", "https://swapi.dev/api/films/");


req.onload = function (e) {
    console.log(req.statusText);

    if (req.status === 200) {
        const data = JSON.parse(req.response);
        console.log(data);

        const episodesList = document.createElement('ul');
        const episodesListItem = document.createElement('li');
        const episodeContainer = document.createElement('div');

        data.results.forEach(item => {
            const titel = document.createElement('h2');
            const id = document.createElement('h2');
            const crawl = document.createElement('p');
            titel.innerText = `${item.title}`;
            id.innerText = `${item.episode_id}`;
            crawl.innerText = `${item.opening_crawl}`
            episodeContainer.append(titel);
            episodeContainer.append(id);
            episodeContainer.append(crawl);
            episodesList.append(episodesListItem);
            episodesListItem.append(episodeContainer);
            document.querySelector('body').append(episodesList);


            item.characters.forEach(elements => {
                const req = new XMLHttpRequest();
                req.open("GET", elements);

                req.onload = function (e) {
                  const dataOne = JSON.parse(req.response);
                    console.log(dataOne, '=1=');
                    if(req.status === 200){
                        titel.after(`${dataOne.name}; `)
                    } else {
                        console.log(req.status);
                        const errorText = document.createElement('h1');
                        errorText.innerText = `ERROR ${req.status}`;
                        errorText.append('body');
                    }
                }
                req.send();

            })

        })



    } else {
        console.log(req.status);
        const errorText = document.createElement('h1');
        errorText.innerText = `ERROR ${req.status}`;
        errorText.append('body');
    }
}


req.send();