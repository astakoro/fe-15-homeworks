class Hamburger {
    constructor(size, stuffing) {
        if ([Hamburger.SIZE_SMALL, Hamburger.SIZE_LARGE].indexOf(size) < 0) {
            throw new HamburgerException('Size is not valid' + ' ' + size.name);
        }
        if ([Hamburger.STUFFING_CHEESE, Hamburger.STUFFING_SALAD, Hamburger.STUFFING_POTATO].indexOf(stuffing) < 0) {
            throw new HamburgerException('Stuffing is not valid' + ' ' + stuffing.name);
        } else {
            this._size = size;
            this._stuffing = stuffing;
            this._toppingList = [];
        }

    }

    getToppings() {
        return this._toppingList;
    }

    addTopping(topping) {
        if (this._toppingList.indexOf(topping) >= 0) {
            throw new HamburgerException('topping has been already added' + ' ' + topping.name);
        }
        this._toppingList.push(topping)
    }

    removeTopping(topping) {
        const toppingIndex = this._toppingList.indexOf(topping);
        if (toppingIndex >= 0) {
            this._toppingList = this._toppingList.slice(toppingIndex, toppingIndex + 1);
            return;
        }
        console.log(topping.name)
        throw new HamburgerException('Topping has never been added');
    }


    getSize() {
        return this._size;
    }

    getStuffing() {
        return this._stuffing;
    }

    calculatePrice() {
        return this._size.price + this._stuffing.price + this._toppingList.reduce((price, topping) => {
            return price + topping.price
        }, 0);
    }

    calculateCalories() {
        return this._size.calories + this._stuffing.calories + this._toppingList.reduce((calories, topping) => {
            return calories + topping.calories
        }, 0);
    }

}

class HamburgerException extends Error {
    constructor(message) {
        super();
        this.message = message;
        this.name = 'HamburgerException';
    }

}


/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = {price: 50, calories: 20, name: 'SIZE_SMALL'}
Hamburger.SIZE_LARGE = {price: 100, calories: 40, name: 'SIZE_LARGE'}
Hamburger.STUFFING_CHEESE = {price: 10, calories: 20, name: 'STUFFING_CHEESE'}
Hamburger.STUFFING_SALAD = {price: 20, calories: 5, name: 'STUFFING_SALAD'}
Hamburger.STUFFING_POTATO = {price: 15, calories: 10, name: 'STUFFING_POTATO'}
Hamburger.TOPPING_MAYO = {price: 20, calories: 5, name: 'TOPPING_MAYO'}
Hamburger.TOPPING_SPICE = {price: 15, calories: 0, name: 'TOPPING_SPICE'}


// //не передали обязательные параметры
// try {
//     var h2 = new Hamburger();
// } catch (error){
//     console.log('something went wrong', error)
// }
// var h2 = new Hamburger(); // => HamburgerException: no size given
//
// // передаем некорректные значения, добавку вместо размера

// var h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE);
// => HamburgerException: invalid size 'TOPPING_SAUCE'
//
// //добавляем много добавок
// var h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// h4.addTopping(Hamburger.TOPPING_MAYO);
// h4.addTopping(Hamburger.TOPPING_MAYO);
// //HamburgerException: duplicate topping 'TOPPING_MAYO'


// маленький гамбургер с начинкой из сыра
// var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// // // добавка из майонеза
// hamburger.addTopping(Hamburger.TOPPING_MAYO);
// // // спросим сколько там калорий
// console.log("Calories: %f", hamburger.calculateCalories());
// // // сколько стоит
// console.log("Price: %f", hamburger.calculatePrice());
// // // я тут передумал и решил добавить еще приправу
// hamburger.addTopping(Hamburger.TOPPING_SPICE);
// // // А сколько теперь стоит?
// console.log("Price with sauce: %f", hamburger.calculatePrice());
// // // Проверить, большой ли гамбургер?
// console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
// // // Убрать добавку
// hamburger.removeTopping(Hamburger.TOPPING_SPICE);
// console.log("Have %d toppings", hamburger.getToppings().length); // 1
//
