/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */
function Hamburger(size, stuffing) {
    if ([Hamburger.SIZE_SMALL, Hamburger.SIZE_LARGE].indexOf(size) < 0) {
        throw new HamburgerException('Size is not valid');
    }
    if ([Hamburger.STUFFING_CHEESE, Hamburger.STUFFING_SALAD, Hamburger.STUFFING_POTATO].indexOf(stuffing) < 0) {
        throw new HamburgerException('Stuffing is not valid');
    }
    this.size = size;
    this.stuffing = stuffing;
    this.toppingList = [];
}

/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = {price: 50, calories: 20}
Hamburger.SIZE_LARGE = {price: 100, calories: 40}
Hamburger.STUFFING_CHEESE = {price: 10, calories: 20}
Hamburger.STUFFING_SALAD = {price: 20, calories: 5}
Hamburger.STUFFING_POTATO = {price: 15, calories: 10}
Hamburger.TOPPING_MAYO = {price: 20, calories: 5}
Hamburger.TOPPING_SPICE = {price: 15, calories: 0}

/**
 * Добавить добавку к гамбургеру. Можно добавить несколько
 * добавок, при условии, что они разные.
 *
 * @param topping     Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.addTopping = function (topping) {
    if (this.toppingList.indexOf(topping) >= 0) {
        throw new HamburgerException('topping has been already added');
    }
    this.toppingList.push(topping)
}

/**
 * Убрать добавку, при условии, что она ранее была
 * добавлена.
 *
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function (topping) {
    const toppingIndex = this.toppingList.indexOf(topping);
    if (toppingIndex >= 0) {
        this.toppingList = this.toppingList.slice(toppingIndex, toppingIndex + 1);
        return;
    }
    throw new HamburgerException('Topping has never been added');
}

/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
Hamburger.prototype.getToppings = function () {
    return this.toppingList;
}

/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function () {
    return this.size;
}

/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function () {
    return this.stuffing;
}

/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */
Hamburger.prototype.calculatePrice = function () {
    return this.size.price + this.stuffing.price + this.toppingList.reduce((price, topping) => {
        return price + topping.price
    }, 0);
}

/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */
Hamburger.prototype.calculateCalories = function () {
    return this.size.calories + this.stuffing.calories + this.toppingList.reduce((calories, topping) => {
        return calories + topping.calories
    }, 0);
}

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером.
 * Подробности хранятся в свойстве message.
 * @constructor
 */
function HamburgerException(message) {
    this.message = message;
    this.name = 'HamburgerException';
}


//не передали обязательные параметры
// try {
//     var h2 = new Hamburger();
// } catch (error){
//     console.log('something went wrong', error)
// }
var h2 = new Hamburger(); // => HamburgerException: no size given

// передаем некорректные значения, добавку вместо размера
var h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE);
//=> HamburgerException: invalid size 'TOPPING_SAUCE'

//добавляем много добавок
var h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
h4.addTopping(Hamburger.TOPPING_MAYO);
h4.addTopping(Hamburger.TOPPING_MAYO);
//HamburgerException: duplicate topping 'TOPPING_MAYO'


// маленький гамбургер с начинкой из сыра
var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// добавка из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
// сколько стоит
console.log("Price: %f", hamburger.calculatePrice());
// я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);
// А сколько теперь стоит?
console.log("Price with sauce: %f", hamburger.calculatePrice());
// Проверить, большой ли гамбургер?
console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
// Убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings().length); // 1

