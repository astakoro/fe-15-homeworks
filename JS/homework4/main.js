class Trello {

    constructor() {
        this.card = document.createElement('div');
        this.box = document.querySelector('.box')
        this.card.classList.add('card');
        this.addBtn = null;
        this.addSort = null;
        this.taskbox = document.createElement('div');
        this.field = null;
        this.fieldBox = null;
        this.fields = [];
        this.createButtons();

    }


    createButtons() {
        this.addBtn = document.createElement('button');
        this.addBtn.innerText = 'ADD CARD';
        this.addBtn.classList.add('addBtn');
        this.addSort = document.createElement('button');
        this.addSort.classList.add('sort');
        this.addSort.innerText = 'SORT';
    }


    createCardTitle() {
        let title = document.createElement('input');
        title.classList.add('titel');
        this.card.prepend(title);
    }


    startCreateTasks() {
        this.createColumn();
        this.createTasksBox();
        this.createCardTitle();
        this.createField();
        this.sortFields();
    }


    createColumn() {
        this.card.append(this.addBtn);
        this.card.append(this.addSort);
        this.box.append(this.card);
    }


    createField() {

        this.addBtn.addEventListener('click', () => {
            this.fieldBox = document.createElement('div');
            this.fieldBox.classList.add('fieldBox');
            this.taskbox.append(this.fieldBox)
            this.field = document.createElement('input');
            this.field.classList.add('field-item');
            this.fieldBox.draggable = true;
            this.fieldBox.append(this.field);
            this.fields.push(this.fieldBox);
            this.createTasksBox()
            this.cleanNotSortTasks();
            this.addSortedTasks();

        });
    }


    createTasksBox() {
        this.taskbox.classList.add('taskbox');
        this.addBtn.before(this.taskbox);
        new DragAndDrop( this.taskbox, 'fieldBox');
        
    }

    
    addSortedTasks() {
        this.fields.forEach((item) => {
            this.taskbox.append(item);
        })
    }

    cleanNotSortTasks() {
        this.fields.forEach((item) => {
            item.remove();
            console.log(this.fields)
        })
    }


    sortFields() {
        this.addSort.addEventListener('click', () => {
       
            this.fields.sort((x, y) => {
                if (x.children[0].value > y.children[0].value) {
                    return 1;
                }
                if (x.children[0].value < y.children[0].value) {
                    return -1;
                }
                return 0;
            })
            this.cleanNotSortTasks();
            this.addSortedTasks();
        })

    }

}


class DragAndDrop {
    constructor(container, draggbleClass) {
        this.container = container;
        this.draggbleClass = draggbleClass;

        this.dragStartHandler = this.dragStartHandler.bind(this);
         this.dragEndHandler = this.dragEndHandler.bind(this);
         this.dragOverHandler = this.dragOverHandler.bind(this);
        this.addDragAndDrop();

    }

    dragStartHandler(event){
        event.target.classList.add("selected");
    }

    dragEndHandler(event){
        event.target.classList.remove("selected");
    }

    dragOverHandler(event){
        event.preventDefault();
console.log(this)
        // Находим перемещаемый элемент
        const activeElement = this.container.querySelector(".selected");
        const currentElement = event.target;
        const isMoveable =
            activeElement !== currentElement &&
            currentElement.classList.contains(this.draggbleClass);

        if (!isMoveable) return;

        const nextElement = this.getNextElement(event.clientY, currentElement);


        if (
            (nextElement && activeElement === nextElement.previousElementSibling) ||
            activeElement === nextElement
        ) {

            return;
        }

        this.container.insertBefore(activeElement, nextElement);
    }

    addDragAndDrop(){
        this.container.addEventListener('dragstart', this.dragStartHandler);
        this.container.addEventListener('dragend', this.dragEndHandler);
        this.container.addEventListener('dragover', this.dragOverHandler);

    }

    getNextElement(cursorPosition, currentElement) {
        const currentElementCoord = currentElement.getBoundingClientRect();
        const currentElementCenter =
            currentElementCoord.y + currentElementCoord.height / 2;


        const nextElement =
            cursorPosition < currentElementCenter
                ? currentElement
                : currentElement.nextElementSibling;

        return nextElement;
    }


}


let button = document.createElement('button');
button.innerText = "Create new Task";
document.querySelector('body').append(button);


button.addEventListener('click', () => {
    let trello = new Trello();
    trello.startCreateTasks();
})


let container = document.createElement('div');
container.classList.add('box');
document.querySelector('body').append(container);


