class API {
    constructor() {

    }

    static USERS = 'https://jsonplaceholder.typicode.com/users';
    static POSTS = 'https://jsonplaceholder.typicode.com/posts';

    getUsers() {
        return fetch(API.USERS)
            .then((response) => response.json())
    }

    getUser(id) {
        return fetch(`${API.USERS}/${id}`)
            .then((response) => response.json())
    }

    getPosts() {
        return fetch(API.POSTS)
            .then((response) => response.json())
    }

    createPost(title, text) {
        return fetch(API.POSTS, {
            method: 'POST',
            body: JSON.stringify({
                title: title,
                body: text,
                userId: 1
            }),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        })
            .then(response => response.json())
    }

    deletePost(postId) {
        return fetch(`${API.POSTS}/${postId}`, {
            method: 'DELETE',
        })
    }

    editPost(postId, title, text, userId) {
        return fetch(`${API.POSTS}/${postId}`, {
            method: 'PUT',
            body: JSON.stringify({
                id: postId,
                title: title,
                body: text,
                userId: userId
            }),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        })
            .then(response => response.json())
    }

}

const api = new API();

// api.getUsers().then((json) => console.log(json))

const createPostButton = document.getElementById('create');
createPostButton.addEventListener('click', () => {
    const modal = new Modal();
    modal.createModal();
    modal.overrideEventListener();
})


api.getPosts()
    .then(posts => {
        api.getUsers()
            .then(users => {
                const postsWithUserCredentials = posts.map((post) => {
                    const currentUser = users.find(user => {
                        return user.id === post.userId;
                    })
                    const firstName = currentUser.name.split(' ')[0];
                    const lastName = currentUser.name.split(' ')[1];

                    return {
                        ...post,
                        firstName: firstName,
                        lastName: lastName,
                        email: currentUser.email
                    }
                })
                postsWithUserCredentials.forEach(post => {
                    const newPost = new Post(post);
                    newPost.render();
                })
            });
    });


class Post {
    constructor({title, body, firstName, lastName, email, id: postId, userId}) {
        this.title = title;
        this.body = body;
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.postId = postId;

        this.container = document.getElementById('posts');
        this.postContainer = document.createElement('div');  //контейнер самого твита
        this.deleteButton = document.createElement('button');
        this.editButton = document.createElement('button');
        this.titlePost = null;
        this.postBody = null;
        // this.emailUser = null;
        this.deletePost = this.deletePost.bind(this);
        this.editPost = this.editPost.bind(this);


    }

    render(isNew = false) {
        // this.postContainer.classList.add('')
        // this.deleteButton = document.createElement('button');

        const nameUser = `<div class="box"><span>${this.firstName}</span><span>${this.lastName}</span><span>${this.email}</span></div>`
        const title = `${this.title}`;
        const body = `${this.body}`;
        // const email = `${this.email}`
        this.postBody = document.createElement('div');
        this.postBody.classList.add('postBody');
        // this.emailUser = document.createElement('span');
        this.titlePost = document.createElement('h2');
        this.postContainer.classList.add('postContainer')
        this.editButton.classList.add('edit');
        this.editButton.innerText = 'EDIT';
        this.deleteButton.classList.add('delete');
        this.deleteButton.innerText = 'DELETE';


        this.titlePost.innerHTML = title;
        this.postBody.innerHTML = body;
        // this.emailUser.innerHTML = email;
        this.postContainer.innerHTML = nameUser;

        // this.postContainer.append(this.emailUser);
        this.postContainer.append(this.titlePost);

        this.postContainer.append(this.postBody);


        this.postContainer.append(this.deleteButton);
        this.postContainer.append(this.editButton);
        isNew ? this.container.prepend(this.postContainer) : this.container.append(this.postContainer)
        // this.container.append(this.postContainer);
        this.deleteButton.addEventListener('click', this.deletePost)
        this.editButton.addEventListener('click', this.editPost)

    }

    deletePost() {
        api.deletePost(this.postId)
            .then(() => this.postContainer.remove())
    }


    editPost() {
        const modal = new Modal(this);
        modal.createModal();

    }


}


class Modal {
    constructor(post) {
        this.post = post;
        this.modalBox = null;
        this.newTitle = null
        this.newBody = null
        this.modalSaveButton = null;
        this.modalCancelButton = null;
        this.createModal = this.createModal.bind(this)
        this.createNewPost = this.createNewPost.bind(this)
        this.editPostHandler = this.editPostHandler.bind(this)
    }


    createNewPost() {
        api.createPost(this.newTitle.value, this.newBody.value)


            .then(post => {
                api.getUser(1)
                    .then(user => {
                        const firstName = user.name.split(' ')[0];
                        const lastName = user.name.split(' ')[1];
                        const postWithUserCredentials = {
                            ...post,
                            firstName: firstName,
                            lastName: lastName,
                            email: user.email
                        }

                        const newPost = new Post(postWithUserCredentials);
                        newPost.render(true)


                    })
            })
        this.modalBox.remove()
    }


    createModal() {
        this.modalBox = document.createElement('div');
        this.modalBox.classList.add('modalBox');
        this.modalBox.classList.add('hidden');
        this.modalSaveButton = document.createElement('button');
        this.modalCancelButton = document.createElement('button');
        this.modalSaveButton.innerText = 'SAVE';
        this.modalCancelButton.innerText = 'CANCEL';
        this.newTitle = document.createElement('textarea');
        this.modalBox.append(this.newTitle);
        this.newBody = document.createElement('textarea');
        this.modalBox.append(this.newBody);
        this.modalBox.append(this.modalSaveButton);
        this.modalBox.append(this.modalCancelButton);

        document.body.append(this.modalBox)
        document.querySelector('.modalBox').classList.remove('hidden');
        this.modalCancelButton.addEventListener('click', () => {
            this.modalBox.remove()
            this.modalBox = null

        })

        this.modalSaveButton.addEventListener('click', this.editPostHandler)


    }

    editPostHandler() {
        this.post.title = this.newTitle.value;
        console.log(this.post.title)
        this.post.titlePost.innerText = this.post.title;
        this.post.body = this.newBody.value;
        this.post.postBody.innerText = this.post.body;
        this.modalBox.remove()

        console.log(this, '=0=')
        api.editPost(this.post.postId, this.newTitle.value, this.newBody.value, this.post.userId)
            .then((response) => console.log(response))
    }

    overrideEventListener() {
        this.modalSaveButton.innerText = 'CREATE'
        this.modalSaveButton.removeEventListener('click', this.editPostHandler)
        this.modalSaveButton.addEventListener('click', this.createNewPost)

    }


}
