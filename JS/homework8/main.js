const button = document.createElement('button');
const box = document.createElement('ul')
button.innerText = 'MY IP'
document.body.append(button);
document.body.append(box)


button.addEventListener('click', checkMyIp);

async function checkMyIp() {
    const response = await fetch('https://api.ipify.org/?format=json')
    const data = await response.json();
    const {ip} = data;
 async function getMyIpParameters(){
     box.innerHTML = '';
     const allParameters = `http://ip-api.com/json/${ip}?fields=continent,country,regionName,city,district&lang=ru`
     let response = await fetch(allParameters);
     const dataOne = await response.json()
     // console.log(dataOne)
     const {continent, country, city, regionName, district } = dataOne;
     const information = `<li>Continent: ${continent}</li> <li>Country: ${country}</li> <li>City: ${city}</li> <li>Region: ${regionName}</li> <li>District:${district}</li>`;
     box.innerHTML = information;
 }


    await getMyIpParameters()

}