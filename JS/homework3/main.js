class Fight {
    static EASY = {name: "easy", time: 1500};
    static MIDDLE = {name: "middle", time: 1000};
    static HARD = {name: "hard", time: 500};

    constructor(complexity) {
        this.complexity = complexity;
        this.userScore = 0;
        this.enemyScore = 0;
        this.timeOutId = null;
        this.activeCell = null;
        this.userClickHandler = this.userClickHandler.bind(this);
        this.enemyClickHandler = this.enemyClickHandler.bind(this);
        this.gameIteration = this.gameIteration.bind(this);
        this.gameOver = false;


    }

    startGame() {
        this.removeBtns()
        this.bildTable()
        this.fightPoints()
        this.gameIteration()

    }


    removeBtns() {
        easyLevel.style.display = 'none'
        middleLevel.style.display = 'none'
        hardLevel.style.display = 'none'
    }

    getRandomCell() {
        let cellArr = document.querySelectorAll('td:not(.mywin):not(.enemywin)');
        let idx = Math.floor(Math.random() * cellArr.length);
        this.activeCell = cellArr[idx];
        this.activeCell.classList.add('try');


    }

    userClickHandler() {
        this.activeCell.classList.remove('try')
        this.activeCell.classList.add('mywin')
        this.userScore++;

        this.activeCell.removeEventListener('click', this.userClickHandler);
        clearTimeout(this.timeOutId);
        this.gameIteration();
    }


    enemyClickHandler() {
        this.activeCell.classList.remove('try')
        this.activeCell.classList.add('enemywin')
        this.enemyScore++;
        this.activeCell.removeEventListener('click', this.userClickHandler);
        this.gameIteration();

    }


    gameIteration() {
        this.checkPoints();
        if (this.gameOver) {
            return;
        }
        this.getRandomCell();
        this.activeCell.addEventListener('click', this.userClickHandler);
        this.timeOutId = setTimeout(this.enemyClickHandler, this.complexity.time);
        this.fightPointsRemove();
        this.fightPoints()

    }


    checkPoints() {

        if (this.userScore > 50 || this.enemyScore > 50) {
            this.endOfTheFight();
            this.gameOver = true;
        }
    }


    bildTable() {
        let field = document.createElement('table'), tr, td, row, cell;
        for (row = 0; row < 10; row++) {
            tr = document.createElement('tr');
            for (cell = 0; cell < 10; cell++) {
                td = document.createElement('td');
                tr.appendChild(td);
            }
            field.appendChild(tr);
        }
        document.querySelector('.box').appendChild(field);
    }


    fightPoints() {
        let userPoints = document.createElement('span');
        let enemyScore = document.createElement('span');
        userPoints.innerText = "USER:" + ' ' + `${this.userScore}`;
        enemyScore.innerText = "ENEMY:" + ' ' + `${this.enemyScore}`;
        document.querySelector('body').append(userPoints, enemyScore);
    }

    fightPointsRemove() {
        document.querySelectorAll('span').forEach(e => e.remove())
    }


    endOfTheFight() {
        let rlb = document.createElement('button');

        if (this.userScore > 50) {
            rlb.innerText = 'TRY AGAIN';
            document.querySelector('body').append(rlb);
            rlb.addEventListener('click', () => {
                window.location.reload();
            })


        } else {
            rlb.innerText = 'TRY AGAIN';
            document.querySelector('body').append(rlb);
            rlb.addEventListener('click', () => {
                window.location.reload();
            })

        }
    }


}

let easyLevel = document.createElement("button");
easyLevel.innerText = 'Easy Level';
let middleLevel = document.createElement("button");
middleLevel.innerText = 'Middle Level';
let hardLevel = document.createElement("button");
hardLevel.innerText = 'Hard Level';
document.querySelector("body").prepend(easyLevel, middleLevel, hardLevel);


easyLevel.addEventListener('click', () => {
    let newFight = new Fight(Fight.EASY)
    newFight.startGame()
})

middleLevel.addEventListener('click', () => {
    let newFight = new Fight(Fight.MIDDLE)
    newFight.startGame()
})

hardLevel.addEventListener('click', () => {
    let newFight = new Fight(Fight.HARD)
    newFight.startGame()
})

