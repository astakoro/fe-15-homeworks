fetch('https://swapi.dev/api/films/')
    .then(response => response.json())
    .then(json => {
        const episodesList = document.createElement('ul');
        const episodesListItem = document.createElement('li');
        const episodeContainer = document.createElement('div');

        json.results.forEach(item => {
            const titel = document.createElement('h2');
            const id = document.createElement('h2');
            const crawl = document.createElement('p');
            titel.innerText = `${item.title}`;
            id.innerText = `${item.episode_id}`;
            crawl.innerText = `${item.opening_crawl}`
            episodeContainer.append(titel);
            episodeContainer.append(id);
            episodeContainer.append(crawl);
            episodesList.append(episodesListItem);
            episodesListItem.append(episodeContainer);
            document.querySelector('body').append(episodesList);

            const char = item.characters.map(e => {
                return fetch(e)
                    .then(response => response.json())
            })

            Promise.all(char)
                .then(element => {
                    element.forEach(el => id.before(el.name + ' ; '))
                })


        })

    })


